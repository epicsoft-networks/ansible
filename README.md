# Ansible

This Docker image is managed and kept up to date by [epicsoft LLC](https://epicsoft.one).

Ansible in Alpine Docker container based on official 'python' image.

* Base image: https://hub.docker.com/_/python
* Dockerfiles: https://gitlab.com/epicsoft-networks/ansible/tree/master
* Repository: https://gitlab.com/epicsoft-networks/ansible/tree/master
* Container-Registry: https://gitlab.com/epicsoft-networks/ansible/container_registry
* Docker Hub: https://hub.docker.com/r/epicsoft/ansible

## Versions

`3.20` `3.20-pluigins` `latest` `latest-pluigins`  
based on `python:3-alpine3.20` image with alpine `3.20.x` - build `weekly`
  - Python version `3.x.x` - depending on base image
  - Ansible version `2.17.2` _(2024-07-20)_ - depending on Python package `ansible`
  - Python package `ansible` latest version - https://pypi.org/project/ansible/
  - Python package `ansible-lint` latest version - https://pypi.org/project/ansible-lint/
  - Python package `hvac` latest version - https://pypi.org/project/hvac/
  - Python package `botocore` latest version - https://pypi.org/project/botocore/
  - Python package `boto3` latest version - https://pypi.org/project/boto3/
  - Python package `resolvelib` latest version - https://pypi.org/project/resolvelib/

`3.19` `3.19-pluigins`  
based on `python:3-alpine3.19` image with alpine `3.19.x` - build `weekly`
  - Python version `3.x.x` - depending on base image
  - Ansible version `2.16.3` _(2024-02-18)_ - depending on Python package `ansible`
  - Python package `ansible` latest version - https://pypi.org/project/ansible/
  - Python package `ansible-lint` latest version - https://pypi.org/project/ansible-lint/
  - Python package `hvac` latest version - https://pypi.org/project/hvac/
  - Python package `botocore` latest version - https://pypi.org/project/botocore/
  - Python package `boto3` latest version - https://pypi.org/project/boto3/
  - Python package `resolvelib` latest version - https://pypi.org/project/resolvelib/

`3.18` `3.18-pluigins`  
based on `python:3.12.2-alpine3.18` image with alpine `3.18.6`
  - Python version `3.12.2` - depending on base image
  - Ansible version `2.16.3` - depending on Python package `ansible`
  - Python package `ansible` version `9.2.0` - https://pypi.org/project/ansible/9.2.0/
  - Python package `ansible-lint` version `24.2.0` - https://pypi.org/project/ansible-lint/24.2.0/
  - Python package `hvac` version `2.1.0` - https://pypi.org/project/hvac/2.1.0/
  - Python package `botocore` version `1.34.44` - https://pypi.org/project/botocore/1.34.44/
  - Python package `boto3` version `1.34.44` - https://pypi.org/project/boto3/1.34.44/
  - Python package `resolvelib` version `0.8.1` - https://pypi.org/project/resolvelib/0.8.1/

`*-pluigins`  
Plugins are installed in an additional image with the suffix `-plugins`
  - `community.general` - https://galaxy.ansible.com/ui/repo/published/community/general/
  - `ansible.posix` - https://galaxy.ansible.com/ui/repo/published/ansible/posix/
  - `ansible.netcommon` - https://galaxy.ansible.com/ui/repo/published/ansible/netcommon/
  - `community.hashi_vault` - https://galaxy.ansible.com/community/hashi_vault
  - `ansible-modules-hashivault` - https://github.com/TerryHowe/ansible-modules-hashivault

## Additional Alpine packages

Alpine packages are installed in the current image in the latest version - https://pkgs.alpinelinux.org/packages
  - sshpass
  - openssh-client
  - openssl
  - rsync
  - nano
  - py-pip
  - py3-setuptools
  - git
  - jq
  - curl
  - yq
  - ca-certificates

## Examples

### Example usage for GitLab CI/CD

`.gitlab-ci.yml`

```yml
stages:
  - deployment

TestDeployment:
  stage: deployment
  image: epicsoft/ansible:latest-plugins
  script:
    - ansible-playbook deployment.yml -i inventory.yml
```

### Example usage for Windows with WSL

A small workaround is necessary under WSL so that the correct permissions can be set.

```bash
docker run --rm -it --name ansible \
  -v $(pwd):/in:ro \
  epicsoft/ansible:latest-plugins \
    sh -c "mkdir /ansible \
      && rsync -a --quiet --exclude='.git' /in/ /ansible/ \
      && chmod -R 700 /ansible \
      && chmod -R 400 /ansible/secrets \
      && cd /ansible \
      && ansible-playbook deployment.yml -i inventory.yml"
```

## License

MIT License see [LICENSE](https://gitlab.com/epicsoft-networks/ansible/blob/master/LICENSE)

Please note that the MIT license only applies to the files created by epicsoft LLC. Other software may be licensed under other licenses.
